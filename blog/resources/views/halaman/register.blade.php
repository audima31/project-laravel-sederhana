<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
    <form action="/submit" method="POST">
        @csrf
        <p>First name:</p>
        <input type="text" name="nama">
        <br>
        <p>Last name:</p>
        <input type="text" name="namaakhir">
 

        <p>Gender:</p>
        <input type="radio" name="Male" id="Male"> Male <br>
        <input type="radio" name="Female" id="Female"> Female <br>
        <input type="radio" name="Other" id="Other"> Other <br>
  

    
        <p>Nationality:</p>
       <select name="Nationality" id="Nationality">
           <option value="ind">Indonesia</option>
           <option value="mly">Malaysia</option>
           <option value="sgp">Singapore</option>
       </select>

 
        <p>Language Spoken:</p>
        <input type="checkbox" name="ind" id="ind">Bahasa Indonesia <br>
        <input type="checkbox" name="mly" id="mly">Melayu <br>
        <input type="checkbox" name="eng" id="eng">English <br>
  

 
        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="SignUp">
   
</form>
</body>
</html>